![Screenshot preview of the theme "Astralis" by glenthemes](https://64.media.tumblr.com/97f40c519249e3d40a68aba82f953c5a/tumblr_p3py4wSbg31ubolzro3_1280.png)  
![Screenshot preview GIF of the navigation links as they cycle through the 12 zodiac constellations, of which each were made with CSS.](https://64.media.tumblr.com/48f0da54afdd6d3cdee266fa9c94c074/tumblr_p3py4wSbg31ubolzro4_1280.gif)

**Theme no.:** 27  
**Theme name:** Astralis  
**Theme type:** Free / Tumblr use  
**Description:** Astrālis, e, adj. astrum, relating to the stars. This is a space-inspired theme featuring 12 zodiac constellations made from CSS, with love (and pain). Color aesthetics inspired by [@jaesthm](https://jaesthm.tumblr.com/post/168178447197/amethyst-one-two-three-four-code-a-minimal) (unfortunately deactivated).  
**Author:** @&hairsp;glenthemes  

**Release date:** 2018-02-06  
**Last updated:** 2023-07-28

**Post:** [glenthemes.tumblr.com/post/171393294514](https://glenthemes.tumblr.com/post/171393294514)  
**Preview:** [glenthpvs.tumblr.com/astralis](https://glenthpvs.tumblr.com/astralis)  
**Download:** [pastebin.com/0iLqgUze](https://pastebin.com/0iLqgUze)  
**Guide:** [glenthemes.tumblr.com/astralis](http://glenthemes.tumblr.com/astralis)
